cube(`Products`, {
  sql: `SELECT * FROM classicmodels.products`,
  
  joins: {
    
  },
  
  measures: {
    buyprice: {
      sql: `${CUBE}.\`buyPrice\``,
      type: `sum`
    }
  },
  
  dimensions: {
    productline: {
      sql: `${CUBE}.\`productLine\``,
      type: `string`
    }
  }
});
